package pages;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class GlobalActionsPage extends PreAndPost{
	
	public GlobalActionsPage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	/*public CreateTaskPage clickNewTask() throws InterruptedException
	{
		Thread.sleep(2000);
		click(locateElement("xpath","//span[text()='New Task']"));
		
		
		return new CreateTaskPage(driver, test);
	}
	
	*/
	public CreateTaskPage clickCreateTask() throws InterruptedException
	{
		Thread.sleep(3000);
		click(locateElement("xpath","//span[text()='New Task']"));
		
		
		return new CreateTaskPage(driver, test);
	}
	
	
	

}
