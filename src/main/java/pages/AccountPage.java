package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class AccountPage extends PreAndPost{

	public AccountPage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}

	public NewAndEditAccountPage clickNewAccount() {
		click(locateElement("xpath", "//div[@title='New']"));
		return new NewAndEditAccountPage(driver, test);
	}

	public AccountPage enterSearchAccount(String SearchAccountName) throws InterruptedException{

		type(locateElement("xpath", "//input[@name='Account-search-input']"), SearchAccountName);
		click(locateElement("xpath", "//input[@name='Account-search-input']/preceding::div/following::span[@class='countSortedByFilteredBy']"));
		Thread.sleep(1000);
		return this;		
	}

	public AccountPage clickMoreAction() {
		click(locateElement("xpath", "(//a[@role='button'][contains(@class,'rowActions')])[1]"));
		return this;
	}

	public NewAndEditAccountPage clickEditAction() {
		click(locateElement("xpath", "(//a[@role='menuitem'])[1]"));
		return new NewAndEditAccountPage(driver, test);
	}

	public AccountPage clickDeleteAction() {
		click(locateElement("xpath", "(//a[@role='menuitem'])[2]"));
		return this;
	}

	public AccountPage clickDeletebutton() {
		click(locateElement("xpath", "//span[text()='Delete']"));
		return this;
	}

	public AccountPage validatePhoneNo(String phoneNoEntered) throws InterruptedException{
		Thread.sleep(1000);
		String resultPhoneNo = getText(locateElement("xpath", "(//td[@role='gridcell'])[4]"));
		resultPhoneNo = (resultPhoneNo.replaceAll("[^a-zA-Z0-9]", ""));
		if(phoneNoEntered.equals(resultPhoneNo)) {
			//System.out.println("Entered Phone No is Matched");
			reportStep("Entered Phone No is Matched", "PASS");
		}else {
			//System.out.println("Entered Phone No is not Matched");
			reportStep("Entered Phone No is not Matched", "FAIL");
		}
		return this;
	}

	public AccountPage validateAccountDelete(String SearchAccountName) {

		List<WebElement> findElement2 = driver.findElements(By.xpath("//div[@data-key='success']//following::span[@data-aura-class='forceActionsText']"));
		int size = findElement2.size();
		System.out.println(size);

		if (size>0) 
		{						
			String Success_Msg = driver.findElement(By.xpath("//div[@data-key='success']//following::span[@data-aura-class='forceActionsText']")).getText();

			if (Success_Msg.equals(SearchAccountName))
			{
				System.out.println("Searched Values is Successsfully Deleted account name is "+SearchAccountName);
			}
		}
		else
		{
			WebElement findElement = driver.findElement(By.xpath("//div[@role='dialog']//following::div[contains(@id, 'content')]"));
			System.out.println(findElement.getText());
		}
		return this;
	}

}
