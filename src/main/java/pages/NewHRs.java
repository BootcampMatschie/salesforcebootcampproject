package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class NewHRs extends PreAndPost{
	
		
	public NewHRs(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}

	public NewHRs Entershift() {
		type(locateElement("xpath", "(//input[@class=' input'])[2]"),"UK Shift");
		System.out.println("entered ukshift successfully");
	    return this;
	}
		
	public NewHRs scrolldown() {
		scrollDown("(//a[@class='select'])[6]");
		System.out.println("scrolldown successfully");
	    return this;
	}
		
	
	public NewHRs EnterGMT() {
		
		WebElement ele = locateElement("xpath","(//a[@class='select'])[6]");
		jsScriptClick(ele);
		System.out.println("selected arrow successfully");
		WebElement element = locateElement("xpath", "//a[text()='(GMT+00:00) Greenwich Mean Time (GMT)']");
		jsScriptClick(element);
		System.out.println("selected gmt successfully");
	    return this;
	}
	
	public WorktypePage ClickSave() throws InterruptedException {
		WebElement element = locateElement("xpath", "(//span[text()='Save'])[3]");
		jsScriptClick(element);
		System.out.println("clicked save successfully");
	    return new WorktypePage(driver, test);
	}

}
