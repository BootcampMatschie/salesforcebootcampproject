package pages;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class AccountDetailsPage extends PreAndPost {

	public AccountDetailsPage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	public AccountDetailsPage validateAccountname(String accountName) {
		String text = getText(locateElement("xpath", "//span[@class='custom-truncate uiOutputText']"));
		if(text.equals(accountName))
		{
			//System.out.println("New Account is created and same Account name is displayed "+text);
			reportStep("New Account is created and same Account name is displayed "+text, "PASS");
		}
		else
		{
			//System.out.println("New Account is created and different Account name is displayed "+text);
			reportStep("New Account is created and different Account name is displayed "+text, "FAIL");
		}
		return this;
	}

}
