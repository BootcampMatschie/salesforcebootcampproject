package pages;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class OrdersPage extends PreAndPost{

	public OrdersPage (EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	public OrdersPage clickNew() {
		click(locateElement("xpath", "//a[@title='New']"));
		return this;
	}
	
	public OrdersPage typeAccountName(String AccountName) {
		
		type(locateElement("xpath", "//input[@title='Search Accounts']"),AccountName);
		jsScriptClick(locateElement("xpath", "//div[@title='"+AccountName+"']"));
		return this;
	}
	
    public OrdersPage selectContract(String ContractNumber) {
		
		type(locateElement("xpath", "//input[@title='Search Contracts']"),ContractNumber);
		jsScriptClick(locateElement("xpath", "//div[@title='"+ContractNumber+"']"));
		//click(locateElement("xpath", "//div[@title='"+ContractNumber+"']"));
		return this;
	}
    
	public OrdersPage selectStatus() {
		click(locateElement("xpath", "//a[text()='Draft']"));
		jsScriptClick(locateElement("xpath", "//a[@title='Activated']"));
		return this;
	}
	
	
	public OrdersPage selectOrderStartDate() {
	    click(locateElement("xpath", "//a[@class='datePicker-openIcon display']"));
		click(locateElement("xpath", "//a[@title='Go to next month']"));
		jsScriptClick(locateElement("xpath", "(//span[text()='10'])"));
		return this;
	}
	
	
	public OrdersPage clickSave() {
		click(locateElement("xpath", "(//span[text()='Save']/parent::button)[2]"));
		return this;
	}
	
	public OrdersPage verifyOrderText() {
		
		String Text = getText(locateElement("xpath", "//span[@class='toastMessage slds-text-heading--small forceActionsText']"));
		
		System.out.println(Text);
		return null;
	}
}