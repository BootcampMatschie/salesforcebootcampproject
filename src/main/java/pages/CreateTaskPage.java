package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class CreateTaskPage extends PreAndPost {

	public CreateTaskPage(EventFiringWebDriver driver, ExtentTest test) {
		// TODO Auto-generated constructor stub

		this.driver = driver;
		this.test = test;
	}

	public CreateTaskPage enterSubject(String subject) {

		try {

			type(locateElement("xpath","//label[text()='Subject']/following::div[4]/input"),subject);
			System.out.println("subjct is entered successfully");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("subjct is not entered successfully");
			e.printStackTrace();
		}

		return this;
	}

	
	public CreateTaskPage selectContact(String contact) {

		try {
			
			//click(locateElement("xpath","//input[@title=\"Search Contacts\"]"));
			type(locateElement("xpath","//input[@title=\"Search Contacts\"]"),contact);
			
			Thread.sleep(2000);
			click(locateElement("xpath","//input[@title='Search Contacts']//following::ul//li//a//div[@title='"+contact+"']"));
			
			System.out.println("Contact is selected successfully");

				} catch (Exception e) {
			// TODO Auto-generated catch block
					System.out.println("Contact is not selected successfully");
				}
		return this;
	}

	public CreateTaskPage selectStatus() {

		try {
			Thread.sleep(3000);

			click(locateElement("xpath","//a[@class='select']"));

			Thread.sleep(3000);

			click(locateElement("xpath", "//a[@title='Waiting on someone else']"));

			System.out.println("Status is selected successfully");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Status is not selected successfully");
			e.printStackTrace();
		}

		return this;
	}

	public CreateTaskPage clickSave() {

		try {
			
			click(locateElement("xpath","(//span[text()='Save'])[2]"));
			System.out.println("Save is clicked successfully");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Save is not clicked successfully");
			e.printStackTrace();
		}

		return this;
	}

	public CreateTaskPage verifyTask(String subject) throws InterruptedException {
		//String actualSuccText = driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']")).getText();

		
		WebElement eleText=driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']"));
		
		Thread.sleep(2000);
		
		String expectedSuccText = "Task" + " " + subject + " " + "was created.";
		
		verifyPartialText(eleText, expectedSuccText);

		
			return this;
	}
	
	public CreateTaskPage clickTasksTab() {

		try {
			WebElement element1 = driver.findElement(By.xpath("//span[text()='Tasks']"));
			jsScriptClick(element1);
			System.out.println("Tasks Tab is clicked successfully");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Tasks Tab is not clicked successfully");
			e.printStackTrace();
		}

		return this;
	}

	
	public CreateTaskPage clickDropdownIconAndSelectRecentlyViewed() {

		try {
			
			click(locateElement("xpath","//a[contains(@class,'slds-button slds-button--reset')]"));
			click(locateElement("xpath","//span[text()='Recently Viewed']"));
			System.out.println("click DropdownIcon And SelectRecentlyViewed");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Not clicked successfully");
			e.printStackTrace();
		}

		return this;
	}

	public CreateTaskPage clickDropdownBootcampTask(String task) {

		try {
			WebElement element2 = driver.findElement(By.xpath("//span[text()='" + task + "']"));
			jsScriptClick(element2);
			
			System.out.println("BootcampTask is clicked successfully");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("BootcampTask is not clicked successfully");
			e.printStackTrace();
		}

		return this;
	}

	
	public CreateTaskPage clickDelete() {

		try {
			
             Thread.sleep(3000);
			
			//click(locateElement("xpath","//a[@title='Delete']"));
			WebElement elementDrop = driver.findElement(By.xpath("//a[contains(@class,'slds-grid slds-grid--vertical-align-center')]"));
			jsScriptClick(elementDrop);

			
			//click(locateElement("xpath","//a[contains(@class,'slds-grid slds-grid--vertical-align-center')]"));

			Thread.sleep(3000);
			
			//click(locateElement("xpath","//a[@title='Delete']"));
			WebElement elementdel = driver.findElement(By.xpath("//a[@title='Delete']"));
			jsScriptClick(elementdel);

			
			Thread.sleep(3000);

			WebElement element3 = driver.findElement(By.xpath("//span[text()='Delete']"));
			jsScriptClick(element3);

			System.out.println("Delete is clicked successfully");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Delete is not clicked successfully");
			e.printStackTrace();
		}

		return this;
	}
	
	public CreateTaskPage verifyTaskAfterDelete(String taskValue) throws InterruptedException {
		//String actualSuccText = driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']")).getText();

		Thread.sleep(2000);
		WebElement eleText1=driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']"));
		
		Thread.sleep(1000);
		
		String expectedSuccText = "Task \"" + taskValue + "\" was deleted. Undo";

		
		verifyPartialText(eleText1, expectedSuccText);

		
			return this;
	}



}
