package pages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class SalesforceTrustPage extends PreAndPost{
	
	public SalesforceTrustPage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}

	public SalesforceTrustPage selectTrustCompliance() {
		click(locateElement("xpath", "//button[@id=\"dropdown-button\"]/.."));
		click(locateElement("xpath", "//span/p[contains(text(),'Compliance')]/.."));
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return this;
	}
	
	public SalesforceTrustPage sortAlphabetically() {
		click(locateElement("xpath", "//button[text()=' Show filters ']//following-sibling::button"));
		return this;
	}	
	
	public SalesforceTrustPage verifyServicesSort() {
		click(locateElement("xpath", "//li[@title='Services']/a[1]"));
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}  
		ArrayList<String> obtainedList = new ArrayList<String>(); 
		List<WebElement> serviceNames = locateElements("xpath", "//div/article//h2");
		for(WebElement names:serviceNames)
        {
           obtainedList.add(names.getText());
        }
        System.out.println("Service List:" + obtainedList);
        ArrayList<String> sortedList = new ArrayList<String>();   
        for(String s:obtainedList)
        {
        	sortedList.add(s);
        }
        Collections.sort(sortedList, String.CASE_INSENSITIVE_ORDER);
        if(sortedList.equals(obtainedList)) 
        {
        	System.out.println("Sorted Alphabetically:" + obtainedList);	
        }
        else 
        {
        	System.out.println("Not Sorted Alphabetically:" + sortedList);
        }
		
		return this;
	}
				
}
