package pages;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class NewAndEditAccountPage extends PreAndPost{

	public NewAndEditAccountPage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}

	public NewAndEditAccountPage enterAccountName(String accountName) {
		type(locateElement("xpath", "(//label[contains(@class,'label inputLabel')]/following-sibling::input)[1]"), accountName);
		return this;
	}

	public NewAndEditAccountPage selectOwnership() {
		click(locateElement("xpath", "(//a[@class='select'])[3]"));
		click(locateElement("xpath", "(//li[@class='uiMenuItem uiRadioMenuItem'])[2]"));
		return this;
	}
	
	
	public NewAndEditAccountPage selectType(){
		click(locateElement("xpath", "(//a[@class='select'])[2]"));
		//Select Type as Technology Partner 
		click(locateElement("xpath", "(//li[@class='uiMenuItem uiRadioMenuItem'])[7]"));
		return this;
	}

	public NewAndEditAccountPage selectIndustry(){
		click(locateElement("xpath", "(//a[@class='select'])[4]"));
		//Select Industry as Healthcare
		click(locateElement("xpath", "//a[@title='Healthcare']"));
		return this;		
	}

	public NewAndEditAccountPage enterBillingAddress(String billAddr){
		type(locateElement("xpath", "//textarea[@placeholder='Billing Street']"), billAddr);
		return this;
	}

	public NewAndEditAccountPage enterShippingAddress(String shipAddr){
		type(locateElement("xpath", "//textarea[@placeholder='Shipping Street']"), shipAddr);
		return this;
	}

	public NewAndEditAccountPage selectCustomerPriority(){
		click(locateElement("xpath","(//a[@class='select'])[5]"));
		click(locateElement("xpath","//a[@title='Low']"));
		return this;
	}

	public NewAndEditAccountPage selectSLA(){
		click(locateElement("xpath","(//a[@class='select'])[6]"));
		click(locateElement("xpath","//a[@title='Silver']"));
		return this;
	}

	public NewAndEditAccountPage selectActive(){
		click(locateElement("xpath","(//a[@class='select'])[8]"));
		click(locateElement("xpath","(//a[@title='No'])[1]"));
		return this;
	}

	public NewAndEditAccountPage enterPhoneno(String phoneNo){
		type(locateElement("xpath","(//label[contains(@class,'label inputLabel')]/following-sibling::input)[2]"), phoneNo);
		return this;
	}
	
	public NewAndEditAccountPage selectUpsellOpportunity() {
		click(locateElement("xpath","(//a[@class='select'])[7]"));
		click(locateElement("xpath","(//a[@title='No'])[2]"));
		return this;
	}

	public AccountDetailsPage saveNewAccount() throws InterruptedException {
		click(locateElement("xpath","(//span[text()='Save'])[2]"));
		Thread.sleep(1000);
		return new AccountDetailsPage(driver,test);
	}
	
	public AccountPage saveEditAccount() throws InterruptedException {
		click(locateElement("xpath","(//span[text()='Save'])[2]"));
		Thread.sleep(1000);
		return new AccountPage(driver,test);
	}

}
