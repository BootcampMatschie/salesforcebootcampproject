package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import pages.WorktypePage;
import lib.selenium.PreAndPost;

public class WorktypePage extends PreAndPost{
	
	public WorktypePage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	public WorktypePage clickNewbutton() {
		click(locateElement("xpath", "//div[text()='New']"));
		System.out.println("clicked New button successfully");
	    return this;
	}
	public WorktypePage entername(String name) {
		type(locateElement("xpath", "//input[@class=' input']"),name);
		System.out.println("entered name successfully");
	    return this;
	}
	public WorktypePage EnterDescription(String Description) {
		type(locateElement("xpath", "//textarea[@class=' textarea']"),Description);
		System.out.println("entered description successfully");
	    return this;
	}
	public WorktypePage clickSearchhrs() {
		click(locateElement("xpath", "//input[@title='Search Operating Hours']"));
		System.out.println("clicked Searchhrs successfully");
	    return this;
	}
	public NewHRs clickNewhrs() {
		click(locateElement("xpath", "//span[@title='New Operating Hours']"));
		System.out.println("clicked Newhrs successfully");
		return new NewHRs(driver, test);
	}
	
	public WorktypePage enterestimatetime(String Estimateddtime) {
		type(locateElement("xpath", "//input[@class='input uiInputSmartNumber']"),Estimateddtime);
		System.out.println("Entered EstimatedTime successfully");
	    return this;
	}
	public VerifyWorktype ClickSave2() {
		click(locateElement("xpath","(//span[text()='Save'])[2]"));
		System.out.println("clicked save successfully");
	    return new VerifyWorktype(driver, test);
	}
	
	public EditWorktypePage viewTable() throws InterruptedException {
		WebElement mytable = locateElement("xpath", "//table[@role='grid']/tbody");
		ArrayList<String> Data = webTable(mytable);
		System.out.println("data " +Data);
		int Data1 = Data.size();
		System.out.println("data " +Data1);
		Thread.sleep(10000);
		WebElement ele = locateElement("xpath","//span[text()='Show Actions']");
		jsScriptClick(ele);
		
		Thread.sleep(1000);
		 System.out.print(true);
		 click(locateElement("xpath", "//a[@title='Edit']"));
		 System.out.println("clicked save successfully");
		
			
		
    	//comparing the text of first row to idetify the 5th column
//    	for(WebElement rows:rows_table) {
//    		if (rows.getText().contains("Select item 1"))
//    		{
//    			//comparing the text of 5th column to click
//    		List <WebElement> Cells = rows.findElements(By.tagName("td"));
//    			for(WebElement Cell:Cells) {
//    					if (Cell.getText().contains("Show Actions"))
//    						Cell.click();
//    				}
//    		}
//    	}
	  //To locate rows of table. 
    	//driver.findElement(By.xpath("//table[@role='grid']/tbody/tr[1]/td[5]")).click();
    	// JavascriptExecutor jse1 = (JavascriptExecutor)driver;
 	   // jse1.executeScript("arguments[0].click()",Row_col5);
 	    System.out.print(true);
 	  	   return new EditWorktypePage(driver, test);
 	  	    
	}

	
	
}
