package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class HomePage extends PreAndPost{
	
	public HomePage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	public HomePage clickAppLauncer() {
		click(locateElement("class", "slds-icon-waffle"));
		return this;
	}
	
	public AppLauncherPage clickViewAll() {
		click(locateElement("xpath", "//button[text()='View All']"));
		return new AppLauncherPage(driver, test);
	}

	public CreatePublishMobileAppPage clickMobilePublisherLearnMore() {
		click(locateElement("xpath", "//span[text()='Learn More']"));
		switchToWindow(1);
		return new CreatePublishMobileAppPage(driver, test);
	}
	
	public SalesforceTrustPage clickSeeSystemGetStarted() {
		List<WebElement> slide = locateElements("xpath", "//div/h2/span");
		for (WebElement x : slide) 
		{	
			if(x.getText().contains("See System Status")) 
			{
				click(locateElement("xpath", "//span[text()='See System Status']/following::button/span[text()='Get Started']"));
			}
			else 
			{
				click(locateElement("xpath", "//div[@class='rightScroll']"));
			}
		}	
		switchToWindow(1);
		return new SalesforceTrustPage(driver, test);
	}

	public HomePage clickGlobalActionsButton() {	 
		click(locateElement("xpath","//ul[@class=\"slds-global-actions\"]//li[3]//a"));
		return this;
	}

	public NewTaskPage clickNewTask() throws InterruptedException {
		click(locateElement("xpath", "//span[text()='New Task']"));
		Thread.sleep(2000);
	    return new NewTaskPage(driver,test);
	}

	
	public GlobalActionsPage clickGlobalActions() throws InterruptedException
	
	{
		Thread.sleep(20000);
		click(locateElement("xpath","//*[@data-key=\'add\']"));
		return new GlobalActionsPage(driver, test);
	}
	
	
	

}
