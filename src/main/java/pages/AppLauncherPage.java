package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import pages.WorktypePage;
import lib.selenium.PreAndPost;

public class AppLauncherPage extends PreAndPost{
	
	public AppLauncherPage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	public ServicePage clickService() {
		click(locateElement("link","Service"));
		return new ServicePage(driver,test);
	}
	

	public AppLauncherPage scrollDown(){
		scrollDown("//p[text()='Refunds']");
		return this;
	}
	
	public RefundsPage clickRefund() {
		click(locateElement("xpath", "//p[text()='Refunds']"));
		return new RefundsPage(driver,test);
	}

	public OrdersPage clickOrders() {
		click(locateElement("xpath", "//p[text()='Orders']"));
		return new OrdersPage(driver,test);
	}

	public WorktypePage clickWorktype() throws InterruptedException {
		
		driver.manage().window().maximize();
		    	Thread.sleep(10000);
		    	WebElement element = locateElement("xpath", "//p[text()='Work Types']");
				jsScriptClick(element);
				System.out.println("clicked worktype successfully");
			    //inspect new button and click
				return new WorktypePage(driver, test);
	}
	
	
	
	public SalesPage clickSales() {
		click(locateElement("xpath", "//p[text()='Sales']"));
		return new SalesPage(driver, test);
	}
		
	
}
