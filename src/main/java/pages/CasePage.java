package pages;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class CasePage extends PreAndPost{ 
	public String getEditStatus;
	public String getEditPriority;
	public String getEditSLAViolation;
	public String getEditCaseOrgin;
	public String EditCaseNum;
	
	public CasePage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	public CasePage clickNew() {  
		click(locateElement("xpath", "//div[text()='New']"));
		return this;
	}
	
	public CasePage clickAccountName(String actName) {
		System.out.println(actName);
		type(locateElement("xpath", "//input[@title='Search Contacts']"),actName);
		click(locateElement("xpath", "//div[@title='"+actName+"']"));
		return this;
	}
	
	public CasePage selectStatus(String Status) {
		click(locateElement("xpath", "//lightning-combobox[@class='slds-form-element']"));
		jsScriptClick(locateElement("xpath", "//span[text()='"+Status+"']"));
		return this;
	}
	
	public CasePage updateCaseOrgin() {
		click(locateElement("xpath", "//span[text()='Case Origin']/parent::span/parent::div/div"));
		click(locateElement("xpath", "//a[text()='Phone']"));
		return this;
	}
	
	public CasePage enterSubject(String Subject) {
		type(locateElement("xpath", "(//span[text()='Subject'])[2]/parent::label/following-sibling::input"),Subject);
		return this;
	}
	
	public CasePage enterDescription(String Description) {
		type(locateElement("xpath", "//span[text()='Description']/parent::label/following-sibling::textarea"),Description);
		return this;
	}
	
	public ViewPage clickSave() {
		click(locateElement("xpath", "(//span[text()='Save']/parent::button)[2]"));
		return new ViewPage(driver, test);
	}
	
	//Edit Case Page
	
	public String getEditedCaseNumber() {
		String CaseNumVal = locateElement("xpath","//h2[@class='inlineTitle slds-p-top--large slds-p-horizontal--medium slds-p-bottom--medium slds-text-heading--medium']").getText();
//		String CaseNumVal = locateElement("xpath","//h2[@class='inlineTitle slds-p-top--large slds-p-horizontal--medium slds-p-bottom--medium slds-text-heading--medium']").getText();
//		System.out.println("Editable CaseNumber " + EditCaseNum); 
//		System.out.println("CaseNumber " + CaseNumVal); 
		String[] EditCaseNumSplit = CaseNumVal.split(" ");
		EditCaseNum = EditCaseNumSplit[1];
		System.out.println("second word is " + EditCaseNumSplit[1]); 
		System.out.println("EditCaseNum " + EditCaseNumSplit[1]); 

		return EditCaseNum;
	}
	
	public CasePage clickStatus(String status) {
		click(locateElement("xpath", "//lightning-combobox[@class='slds-form-element']"));
		jsScriptClick(locateElement("xpath", "//span[text()='"+status+"']"));
		return this;
	}
	
	public CasePage clickPriority(String priority) {
		click(locateElement("xpath", "//span[text()='Priority']/parent::span/parent::div/div"));
		click(locateElement("xpath", "//a[text()='"+priority+"']"));
		return this;
	}
	
	public CasePage clickSLAviolation(String SLAviolation) {
		click(locateElement("xpath", "//span[text()='SLA Violation']/parent::span/parent::div/div"));
		click(locateElement("xpath", "//a[text()='"+SLAviolation+"']"));
		return this;
	}
	
	public CasePage clickCaseOrigin(String CaseOrigin) {
		click(locateElement("xpath", "//span[text()='Case Origin']/parent::span/parent::div/div"));
		click(locateElement("xpath", "//a[text()='"+CaseOrigin+"']"));
		return this;
	}
	
	public SalesPage clickEditSave() {
		click(locateElement("xpath", "(//span[text()='Save']/parent::button)[2]"));
		return new SalesPage(driver, test);
	}
	
	public CasePage getStatusValue() {
		click(locateElement("xpath", "//lightning-combobox[@class='slds-form-element']"));
		String AttributValue ="data-value";
		getEditStatus = getText(locateElement("xpath", "(//span[@class='slds-media__body'])[3]"));
		click(locateElement("xpath", "//lightning-combobox[@class='slds-form-element']"));
		return this;
	}
	
	public CasePage getPriorityValue() {
		getEditPriority = getText(locateElement("xpath","//span[text()='Priority']/parent::span/parent::div/div//a[@aria-required]"));
		return this;
	}
	
	public CasePage getSLAviolationValue() {
		getEditSLAViolation = getText(locateElement("xpath","//span[text()='SLA Violation']/parent::span/parent::div/div//*[@aria-required]"));
		return this;
	}
	
	public CasePage getCaseOriginValue() {
		getEditCaseOrgin = getText(locateElement("xpath","//span[text()='Case Origin']/parent::span/parent::div/div//a[@aria-required]"));
		return this;
	}
	
	public SalesPage clickCancel(String status,String priority,String SLAviolation,String CaseOrgin) {
		if(getEditStatus.equals(status) && getEditPriority.equals(priority) && getEditCaseOrgin.equals(CaseOrgin) && getEditSLAViolation.equals(SLAviolation)) {
			System.out.println("SFO141 - Test Case Passed");
			System.out.print("Case is Edited Successfully  and Status is working");
		}else {
			System.out.println("SFO141 - Test Case Failed");
		}
		click(locateElement("xpath","(//span[text()='Cancel']/parent::button)[2]"));
		return new SalesPage(driver, test);
		
	}
}
