package pages;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class NewEventPage extends PreAndPost {

	
	public NewEventPage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	public NewEventPage typeStartTime(String startTime) {
		try {
			type(locateElement("xpath","(//input[@role=\"textbox\"])[3]"),startTime);
			System.out.println("Start time is entered successfully");
		} catch (Exception e) {
			System.out.println("Start time is not entered and some exception occurs");
			e.printStackTrace();
		}
		return this;
		
	}
	public NewEventPage typeendTime(String endTime) {
		try {
			type(locateElement("xpath","(//input[@role=\"textbox\"])[4]"),endTime);
			System.out.println("End time is entered successfully");

		} catch (Exception e) {
			System.out.println("End time is not entered and some exception occurs");
			e.printStackTrace();
		}
		return this;
		
	}
	public NewEventPage typeSubject(String subject) {
		type(locateElement("xpath","(//input[@role=\"textbox\"])[2]"),subject);
		return this;
		
	}
	public CalendarPage clickSaveButton() throws InterruptedException {
		click(locateElement("xpath","//span[text()='Save']"));
	    Thread.sleep(2000);   
		return new CalendarPage(driver, test);
		
	}
}
