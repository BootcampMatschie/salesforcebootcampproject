package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class EditWorktypePage extends  PreAndPost{
	
	public EditWorktypePage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	public EditWorktypePage Scrooltoelement() throws InterruptedException {
		scrollDown("//input[@class='input uiInputSmartNumber']");
		System.out.println("scrolldown successfully");
	    return this;
	}
	
	public EditWorktypePage EnterStart(String start) {
		type(locateElement("xpath", "//input[@class='input uiInputSmartNumber']"),start);
		System.out.println("entered start successfully");
	    return this;
	}
	
	public EditWorktypePage EnterEnd(String end) {
		type(locateElement("xpath", "(//input[@class='input uiInputSmartNumber'])[4]"),end);
		System.out.println("entered end successfully");
	    return this;
	}
	public WorktypePage Clickeditsave() {
		click(locateElement("xpath", "(//span[text()='Save'])[2]"));
		System.out.println("click save successfully");
	    return new WorktypePage(driver, test);
	}
}
