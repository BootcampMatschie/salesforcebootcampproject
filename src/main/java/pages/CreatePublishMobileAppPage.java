package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class CreatePublishMobileAppPage  extends PreAndPost{
	
	public CreatePublishMobileAppPage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}

	public CreatePublishMobileAppPage verifyTabs() {
		List<WebElement> tabNames = locateElements("xpath", "//div[@id='globalnavbar-header-container']//nav");
		System.out.println("List of tabs");
		for(int i=0; i<tabNames.size();i++)
		{
			WebElement names = tabNames.get(i);
			System.out.println(names.getText());
		}
		return this;
	}
	
	
	
}
