package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class SalesPage extends PreAndPost{
	//Create Case
	public SalesPage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	public SalesPage clickMore()   { 
		click(locateElement("xpath", "//span[text()='More']"));
		return this;
		
	}
	
	public CasePage clickCase() {
		jsScriptClick(locateElement("xpath", "//span[@class='slds-truncate']/span[text()='Cases']"));
		return new CasePage(driver, test);
	}
	
	//Edit Case
	public SalesPage clickEditCase() {
		jsScriptClick(locateElement("xpath", "//span[@class='slds-truncate']/span[text()='Cases']"));
		return this;
	}
	
	public SalesPage searchName(String CaseName) throws InterruptedException {
		System.out.println("Search CaseName : " + CaseName); 
		type(locateElement("xpath", "//input[@name='Case-search-input']"),CaseName);
		Thread.sleep(10000);
		click(locateElement("xpath", "//input[@name='Case-search-input']"));		
		click(locateElement("xpath","//input[@name='Case-search-input']/preceding::div/following::span[@class='countSortedByFilteredBy']"));
		Thread.sleep(10000);
		return this;
	}
	
	public SalesPage searchCaseNumber(String EditCaseNum) throws InterruptedException {
		System.out.println("SearchBox CaseNum : " + EditCaseNum);
		type(locateElement("xpath", "//input[@name='Case-search-input']"),EditCaseNum);
		click(locateElement("xpath", "//input[@name='Case-search-input']"));
		Thread.sleep(10000);
		click(locateElement("xpath","//input[@name='Case-search-input']/preceding::div/following::span[@class='countSortedByFilteredBy']"));
		return this;
	}
	
	public CasePage clickEdit() throws InterruptedException { 
		Thread.sleep(10000);
		jsScriptClick(locateElement("xpath", "//span[text()='Show Actions']"));
		click(locateElement("xpath", "//a[@title='Edit']"));		
		return new CasePage(driver, test);
	}
	
	public AccountPage clickAccountsTab() {
		jsScriptClick(locateElement("xpath", "//span[text()='Accounts']"));
		return new AccountPage(driver, test);
	}
	
	public ServicePage clickContact() {
		click(locateElement("xpath", "//span[text()='Contacts']"));
		System.out.println("Contact");
		return new ServicePage(driver, test);
	
	}
	
	public CalendarPage clickCalendar()  {	
		try {
			jsScriptClick(locateElement("xpath","(//span[text()='Calendar'])[2]"));
			System.out.println("Calendar option is clicked successfully");
			Thread.sleep(2000);

		} catch (Exception e) {
			System.out.println("Calendar option is not clicked");
			e.printStackTrace();
		}
		return new CalendarPage(driver,test);
	}
	
}
