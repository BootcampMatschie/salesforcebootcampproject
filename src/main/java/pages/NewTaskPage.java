package pages;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class NewTaskPage extends PreAndPost {
	
	
	public NewTaskPage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	
	
	
	public NewTaskPage typeContactInNameInputField(String contactName) {
		
		try {      
			type(locateElement("xpath","//input[@placeholder='Search Contacts...']"),contactName);
			System.out.println("Searching contact name in Input filed");

		} catch (Exception e) {
			System.out.println("Search Contact name failed");
			e.printStackTrace();
		}
		return this;
	}
	
	public NewTaskPage selectFirstContactNameDisplayed(String contactName) {
		
		try {
			click(locateElement("xpath","(//div[@title='"+contactName+"'])[1]"));
			System.out.println("Selected first contact name clicked");

		} catch (Exception e) {
			System.out.println("Selected first contact name not clicked");
			e.printStackTrace();
		}
		return this;
	}
	public NewTaskPage clickSave() {
		
		try {
			click(locateElement("xpath","(//span[text()='Save'])[2]"));
			System.out.println("Save Button Clicked");

		} catch (Exception e) {
			System.out.println("Save Button Not Clicked");
			e.printStackTrace();
		}
		return this;
		
	}
	public TaskPage clickOnTaskCreatedLink() {
		
		try {
			click(locateElement("xpath","//a[@title=\"Task\"]"));
			Thread.sleep(1000);
			System.out.println("Clicked on TaskCreatedLink");

		} catch (Exception e) {
			System.out.println("Clicking TaskCreatedLink Failed");
			e.printStackTrace();
		}
		return new TaskPage(driver,test);
		
	}
}
