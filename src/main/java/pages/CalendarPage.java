package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class CalendarPage extends PreAndPost {
	
	
	public CalendarPage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}

	public NewEventPage clickNewEvent() throws InterruptedException {
		click(locateElement("xpath","//button[text()='New Event']"));
		Thread.sleep(2000);
		return new NewEventPage(driver,test);
	}
	
	public CalendarPage scrollInCalendar() throws InterruptedException {		
		scrollDown("//span[text()='8am']");
		Thread.sleep(1000);
		return this;
	}

	public CalendarPage moveToElement(String subject) throws InterruptedException {		
		MoveToElement("//div[text()='10�11am']/../..//a[text()='"+subject+"']");
		Thread.sleep(2000);
		return this;
	}
	
	public CalendarPage clickDelete() {
		click(locateElement("xpath","//a/div[text()='Delete']"));
		return this;
	}
	public CalendarPage clickDeleteConfirmationPopup() throws InterruptedException {
		click(locateElement("xpath","//button/span[text()='Delete']"));
		Thread.sleep(1000);
		return this;
	}
   public CalendarPage verifyDeleteEvent(String subject) {
	  //String deleteEventText = getText(locateElement("xpath","//span[contains(text(),'"+subject+"')]"));
	  //System.out.println(deleteEventText);
	   WebElement ele = locateElement("xpath","//span[@class=\"toastMessage slds-text-heading--small forceActionsText\"]");
		if (ele.isDisplayed()) {
			System.out.println("Event is deleted that was displayed between time 10 AM to 11AM-Verification Successful");	

		} else 
		{
			System.out.println("Event is not deleted-Verification unsuccessful");	
		}	
		return this;
	   
   }
}
