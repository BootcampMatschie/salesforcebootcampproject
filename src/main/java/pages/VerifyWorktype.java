package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class VerifyWorktype extends PreAndPost{
	
		
	public VerifyWorktype(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	public VerifyWorktype VerifyText() {
		WebElement element = locateElement("xpath", "//span[text()='NEW']");
		String n = "NEW";
		verifyExactText(element,n);
		System.out.println("matching");
	    return this;
	}
}
