package pages;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class ViewPage extends PreAndPost{ 
	public String CreateCaseNum;

	public ViewPage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	public ViewPage getCreatedCaseNumber() throws InterruptedException {
		Thread.sleep(10000);
		CreateCaseNum = getText(locateElement("xpath","(//li//span[text()='Case Number']/following::span[@class='uiOutputText'])[2]"));
		System.out.println("CaseNumber " + CreateCaseNum); 
		return this;
	}
	
	public ViewPage clickCase() {
		jsScriptClick(locateElement("xpath", "(//span[text()='Cases'])[1]"));
		return this;
	}
	
	public ViewPage searchCaseNumber() throws InterruptedException {
		System.out.println(CreateCaseNum); 
		type(locateElement("xpath", "//input[@name='Case-search-input']"),CreateCaseNum);		
		click(locateElement("xpath", "//input[@name='Case-search-input']"));
		click(locateElement("xpath","//input[@name='Case-search-input']/preceding::div/following::span[@class='countSortedByFilteredBy']"));
		Thread.sleep(10000);
		return this;
	}
	
	public ViewPage getCountRow() {
	List<WebElement> RowCount = locateElements("xpath", "//table[@data-aura-class ='uiVirtualDataTable']/tbody/tr");
	int count = RowCount.size();
	if(count == 1) {
		System.out.println("New Case Created");
		System.out.println(" SFO-140 Testcase Passed");
	}else {
		System.out.println("New Case  not Created");
		System.out.println(" SFO-140 Testcase Failed");
	}  
	return this;
}
	
}
