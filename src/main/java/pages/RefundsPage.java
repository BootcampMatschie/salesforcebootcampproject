package pages;

	import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

	import com.aventstack.extentreports.ExtentTest;

	import lib.selenium.PreAndPost;

	public class RefundsPage extends PreAndPost{

		public  RefundsPage (EventFiringWebDriver driver, ExtentTest test) {
			this.driver = driver;
			this.test = test;
		}
		
		public RefundsPage clickNew() {
			click(locateElement("xpath", "//a[@title='New']"));
			return this;
		}
		
		public RefundsPage typeAccountName(String AccountName) {
			
			type(locateElement("xpath", "//input[@title='Search Accounts']"),AccountName);
			click(locateElement("xpath", "//div[@title='"+AccountName+"']"));
			return this;
		}
		
		public RefundsPage selectStatus() {
			click(locateElement("xpath", "(//a[@class='select'])[1]"));
			jsScriptClick(locateElement("xpath", "//a[@title='Canceled']"));
			return this;
		}
		
		public RefundsPage typeAmount(String Amount) {
			
			type(locateElement("xpath", "//input[@class='input uiInputSmartNumber']"),Amount);
			return this;
		}
		
		public RefundsPage selectType() {
			click(locateElement("xpath", "(//a[contains(text(),'None')])[1]"));
			jsScriptClick(locateElement("xpath", "//a[@title='Referenced']"));
			return this;
		}
		
		public RefundsPage selectProcessingMode() {
			click(locateElement("xpath", "(//a[contains(text(),'None')])[1]"));
			jsScriptClick(locateElement("xpath", "//a[@title='External']"));
			return this;
		}
		
		public RefundsPage clickSave() {
			click(locateElement("xpath", "(//span[text()='Save']/parent::button)[2]"));
			return this;
		}
		
		public RefundsPage verifyText() {
			
			String Text = getText(locateElement("xpath", "//span[@class='toastMessage slds-text-heading--small forceActionsText']"));
			
			System.out.println(Text);
			return null;
			
		}
	
	
	}



