package pages;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class TaskPage extends PreAndPost {
	public TaskPage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	
	public TaskPage clickEditComments() {
		
		try {
			//scrollDown(locateElement("//button[@title=\"Edit Comments\"]"));
			click(locateElement("xpath","//button[@title=\"Edit Comments\"]"));
			Thread.sleep(1000);
			System.out.println("EditComments Button clicked");


		} catch (Exception e) {
			System.out.println("EditComments Button not Clicked");
			e.printStackTrace();
		}
	return this; 

	}
	
	public TaskPage Scrolldownpage() {
		
		try {
			scrollDown("//button[@title=\"Edit Comments\"]");
			Thread.sleep(1000);
			System.out.println("Page Scrolled down to Comments");

		} catch (Exception e) {
			System.out.println("Page not Scrolled down to Comments");
			e.printStackTrace();
		}
		return this;
	}
	
	public TaskPage typeComments(String comments) {
		
		try {
			type(locateElement("xpath","(//*[@role=\"textbox\"])[3]"),comments);
			Thread.sleep(1000);
			System.out.println("Comments are entered in the input field");
		} catch (Exception e) {
			System.out.println("Comments are not entered in the input field");
			e.printStackTrace();
		}				
		return this;

	}
	
	public TaskPage clickSaveInTaskPage() {
		try {
			click(locateElement("xpath","//button[@title=\"Save\"]"));
			System.out.println("Save button clicked in TaskPage");

		} catch (Exception e) {
			System.out.println("Save button not clicked in TaskPage");
			e.printStackTrace();
		}

		return this;

	}
	
	public TaskPage verifyErrorMessage(String subjectTextexpected) {
		try {
			String subjecttextactual = locateElement("xpath","//div[text()='Complete this field.']").getText();
			System.out.println(subjecttextactual);
			if(subjecttextactual.equals(subjectTextexpected)) {
				System.out.println("Expected Result Complete this field alert message is displayed for the Subject field-Verification Successful");
			}
			else{
				System.out.println("Expected Result Complete this field alert message is not displayed for the Subject field");
			}
			System.out.println("Error message verified");

		} catch (Exception e) {
			System.out.println("Error message not verified");
			e.printStackTrace();
		}
		return this;
		
	}
}
