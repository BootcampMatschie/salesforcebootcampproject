package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.AppLauncherPage;
import pages.CalendarPage;
import pages.HomePage;
import pages.LoginPage;
import pages.NewEventPage;
import pages.SalesPage;

public class SFO66_DeleteEvent extends PreAndPost {

	@BeforeTest
	public void setData() {
		testCaseName = "SFO66";
		testDescription = "Delete Event";
		authors = "Saran";
		category = "smoke";
		nodes = "Service";
		dataSheetName="																																																																	DeleteEvent";
	}

@Test(dataProvider="fetchData")
  public void deleteEvent(String username,String password,String startTime, String endTime,String subject) throws InterruptedException {
		new LoginPage(driver, test)
		.typeUserName(username)
		.typePassword(password)
		.clickLogIn()	
		
		.clickAppLauncer().clickViewAll().clickSales()
		.clickMore().
		 clickCalendar()
		.clickNewEvent().typeStartTime(startTime)
		.typeSubject(subject).typeendTime(endTime).clickSaveButton()
		.scrollInCalendar().moveToElement(subject).clickDelete().clickDeleteConfirmationPopup()
		.verifyDeleteEvent(subject);	
		}

}
