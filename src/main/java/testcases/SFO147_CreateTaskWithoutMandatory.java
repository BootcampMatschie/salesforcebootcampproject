package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.HomePage;
import pages.LoginPage;
import pages.NewTaskPage;
import pages.TaskPage;

public class SFO147_CreateTaskWithoutMandatory extends PreAndPost{
	
	@BeforeTest
	public void setData() {
		testCaseName = "SFO147";
		testDescription = "CreateTaskWithoutMandatoryFields";
		authors = "Saran";
		category = "smoke";
		nodes = "Service";
		dataSheetName="TaskCreate";
	}

@Test(dataProvider="fetchData")
  public void createTask(String username,String password,String contact, String comments,String subjectTextexpected) throws InterruptedException {
		new LoginPage(driver, test)
		.typeUserName(username)
		.typePassword(password)
		.clickLogIn();
		
		new HomePage(driver, test)
		.clickGlobalActionsButton().clickNewTask();
		
		new NewTaskPage(driver,test)
		.typeContactInNameInputField(contact).selectFirstContactNameDisplayed(contact)
		.clickSave().clickOnTaskCreatedLink();
		
		new TaskPage(driver,test).
		Scrolldownpage().clickEditComments().typeComments(comments)
		.clickSaveInTaskPage().verifyErrorMessage(subjectTextexpected);
		
		
	  
	  
  }
		
		
	

}
