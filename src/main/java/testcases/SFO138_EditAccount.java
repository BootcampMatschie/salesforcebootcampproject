package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.LoginPage;

public class SFO138_EditAccount extends PreAndPost{

	@BeforeTest
	public void setData() {
		testCaseName = "SFO138_EditAccount";
		testDescription = "Edit Account";
		authors = "Krishnaprakash";
		category = "smoke";
		nodes = "Edit Accounts";
		dataSheetName="EditAccount";
	}
		
	@Test(dataProvider = "fetchData",dependsOnMethods = {"testcases.SFO137_CreateAccount.createAccount"})
	public void editAccount(String SearchAccountName, String billAddr, String shipAddr, String phoneNo) throws InterruptedException {
		new LoginPage(driver, test)
		.typeUserName("matschie@testleaf.com")
		.typePassword("Bootcamp$123")
		.clickLogIn()
		.clickAppLauncer()
		.clickViewAll()
		.clickSales()
		.clickAccountsTab()
		.enterSearchAccount(SearchAccountName)
		.clickMoreAction()
		.clickEditAction()
		.selectType()
		.selectIndustry()
		.enterBillingAddress(billAddr)
		.enterShippingAddress(shipAddr)
		.selectCustomerPriority()
		.selectSLA()
		.selectActive()
		.enterPhoneno(phoneNo)
		.selectUpsellOpportunity()
		.saveEditAccount()
		.validatePhoneNo(phoneNo);
		
	}
}

