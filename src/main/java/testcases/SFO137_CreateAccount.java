package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.LoginPage;

public class SFO137_CreateAccount extends PreAndPost{

	@BeforeTest
	public void setData() {
		testCaseName = "SFO137_CreateAccount";
		testDescription = "Create Account";
		authors = "Krishnaprakash";
		category = "smoke";
		nodes = "Create Accounts";
		dataSheetName="CreateAccount";
	}
		
	@Test(dataProvider = "fetchData")
	public void createAccount(String accountName) throws InterruptedException {
		new LoginPage(driver, test)
		.typeUserName("matschie@testleaf.com")
		.typePassword("Bootcamp$123")
		.clickLogIn()
		.clickAppLauncer()
		.clickViewAll()
		.clickSales()
		.clickAccountsTab()
		.clickNewAccount()
		.enterAccountName(accountName)
		.selectOwnership()
		.saveNewAccount()
		.validateAccountname(accountName);
		
	}
}
