package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.CasePage;
import pages.LoginPage;

public class SFO141_EditCase extends PreAndPost{
	
	@BeforeTest
	public void setData() { 
		testCaseName = "SFO141_EditCase";
		testDescription = "EditCase";
		authors = "Gomathi";
		category = "smoke";
		nodes = "Sales";
		dataSheetName = "EditCase";
	} 
		
	@Test(dataProvider = "fetchData")
	public void editCase(String Subject,String status,String priority,String SLAviolation,String CaseOrgin) throws InterruptedException {
		String EditCaseNum = new LoginPage(driver, test)
		.typeUserName("matschie@testleaf.com")
		.typePassword("Bootcamp$123")
		.clickLogIn()
		.clickAppLauncer()
		.clickViewAll()
		.clickSales()
		.clickMore()
		.clickEditCase()
		.searchName(Subject)
		.clickEdit()
		.getEditedCaseNumber();
		new CasePage(driver, test)
		.clickStatus(status)
		.clickPriority(priority)
		.clickSLAviolation(SLAviolation)
		.clickCaseOrigin(CaseOrgin)
		.clickEditSave()
		.searchCaseNumber(EditCaseNum)
		.clickEdit()
		.getStatusValue()
		.getPriorityValue()
		.getSLAviolationValue()
		.getCaseOriginValue()
		.clickCancel(status,priority,SLAviolation,CaseOrgin);
		
	}
	
}
