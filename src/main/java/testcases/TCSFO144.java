package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import lib.selenium.PreAndPost;
import pages.LoginPage;

public class TCSFO144 extends PreAndPost{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC144";
		dataSheetName="TC144";
		testDescription = "Login Page";
		authors = "sarath";
		category = "smoke";
		nodes = "Service";
	}
		
	@Test(dataProvider = "fetchData")
	public void createNewTask(String userName,String password,String subject,String contact) throws InterruptedException {
		new LoginPage(driver, test)
		.typeUserName(userName)
		.typePassword(password)
		.clickLogIn()
		.clickGlobalActions()
		.clickCreateTask()
		.enterSubject(subject)
		.selectContact(contact)
		.selectStatus()
		.clickSave()
		.verifyTask(subject);
		
		
		
		
	}
	

}
