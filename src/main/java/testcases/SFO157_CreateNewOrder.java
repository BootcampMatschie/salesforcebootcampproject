package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.LoginPage;

public class SFO157_CreateNewOrder extends PreAndPost{
	
	@BeforeTest
	public void setData() {
		testCaseName = "SFO157_CreateNewOrder";
		testDescription = "Create New Order";
		authors = "Haripriya";
		category = "smoke";
		nodes = "Service";
		dataSheetName = "NewOrder";
	}
		
	@Test (dataProvider="fetchData")
	public void newRefund(String Username,String Password,String AccountName,String ContractNumber) {
		
		System.out.println(AccountName);
		System.out.println(ContractNumber);
		
		new LoginPage(driver, test)
		.typeUserName(Username)
		.typePassword(Password)
		.clickLogIn()
		.clickAppLauncer()
		.clickViewAll()
		.scrollDown()
		.clickOrders()
		.clickNew()
		.typeAccountName(AccountName)
		.selectContract(ContractNumber)
		.selectStatus()
		.selectOrderStartDate()
		.clickSave();
		
	}
	}