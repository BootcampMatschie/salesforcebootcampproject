package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import lib.selenium.PreAndPost;
import pages.LoginPage;

public class CreateWorktype extends PreAndPost{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC001";
		testDescription = "Login Page";
		authors = "sarath";
		category = "smoke";
		nodes = "Service";
		dataSheetName = "Worktype";
	}
	
		
      @Test(dataProvider = "fetchData")
	public void login(String name,String Description,String Estimatedtime) throws InterruptedException {
		new LoginPage(driver, test)
		.typeUserName("makaia@testleaf.com")
		.typePassword("Bootcamp$123")
		.clickLogIn()
		.clickAppLauncer()
		.clickViewAll()
		.clickWorktype()
		.clickNewbutton()
		.entername(name)
		.EnterDescription(Description)
		.clickSearchhrs()
		.clickNewhrs()
		.Entershift()
		.scrolldown()
		.EnterGMT()
		.ClickSave()
		.enterestimatetime(Estimatedtime)
		.ClickSave2()
		.VerifyText();
		
		
	}
	

}
