package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.LoginPage;

public class TC_NewRefund extends PreAndPost{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC_NewRefund";
		testDescription = "Create Refund";
		authors = "Haripriya";
		category = "smoke";
		nodes = "Service";
		dataSheetName = "NewRefund";
	}
		
	@Test (dataProvider="fetchData")
	public void newRefund(String Username,String Password,String AccountName,String Amount) {
		new LoginPage(driver, test)
		.typeUserName(Username)
		.typePassword(Password)
		.clickLogIn()
		.clickAppLauncer()
		.clickViewAll()
		.scrollDown()
		.clickRefund()
		.clickNew()
		.typeAccountName(AccountName)
		.selectStatus()
		.typeAmount(Amount)
		.selectType()
		.selectProcessingMode()
		.clickSave();
		
	}
	}