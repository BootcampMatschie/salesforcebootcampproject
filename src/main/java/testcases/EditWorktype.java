package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import lib.selenium.PreAndPost;
import pages.LoginPage;

public class EditWorktype extends PreAndPost{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC001";
		testDescription = "Login Page";
		authors = "sarath";
		category = "smoke";
		nodes = "Service";
		dataSheetName = "editworktype";
	}
	
		
      @Test(dataProvider = "fetchData")
	public void login(String start,String end) throws InterruptedException {
		new LoginPage(driver, test)
		.typeUserName("makaia@testleaf.com")
		.typePassword("Bootcamp$123")
		.clickLogIn()
		.clickAppLauncer()
		.clickViewAll()
		.clickWorktype()
		.viewTable()
		.Scrooltoelement()
		.EnterStart(start)
		.EnterEnd(end)
		.Clickeditsave();
		
		
		
		
	}
	

}
