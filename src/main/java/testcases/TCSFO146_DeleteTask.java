package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import lib.selenium.PreAndPost;
import pages.CreateTaskPage;
import pages.LoginPage;

public class TCSFO146_DeleteTask extends PreAndPost{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC146";
		dataSheetName="TC146";
		testDescription = "DeleteTask";
		authors = "saranya";
		category = "smoke";
		nodes = "Service";
	}
		
	@Test(dataProvider = "fetchData")
	public void deleteTask(String userName,String password,String task) throws InterruptedException {
		new LoginPage(driver, test)
		.typeUserName(userName)
		.typePassword(password)
		.clickLogIn()
		.clickAppLauncer()
		.clickViewAll()
		.clickSales();
		new CreateTaskPage(driver, test)
		.clickTasksTab()
		.clickDropdownIconAndSelectRecentlyViewed()
		.clickDropdownBootcampTask(task)
		.clickDelete()
		.verifyTaskAfterDelete(task);
		
		
		
		
		
	}
	

}
