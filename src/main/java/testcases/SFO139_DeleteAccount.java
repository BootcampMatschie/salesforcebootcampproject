package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.LoginPage;

public class SFO139_DeleteAccount extends PreAndPost{

	@BeforeTest
	public void setData() {
		testCaseName = "SFO139_DeleteAccount";
		testDescription = "Delete Account";
		authors = "Krishnaprakash";
		category = "smoke";
		nodes = "Delete Accounts";
		dataSheetName="CreateAccount";
	}
		
	@Test(dataProvider = "fetchData",dependsOnMethods = {"testcases.SFO137_CreateAccount.createAccount","testcases.SFO138_EditAccount.editAccount"})
	public void deleteAccount(String SearchAccountName) throws InterruptedException {
		new LoginPage(driver, test)
		.typeUserName("matschie@testleaf.com")
		.typePassword("Bootcamp$123")
		.clickLogIn()
		.clickAppLauncer()
		.clickViewAll()
		.clickSales()
		.clickAccountsTab()
		.enterSearchAccount(SearchAccountName)
		.clickMoreAction()
		.clickDeleteAction()
		.clickDeletebutton()
		.validateAccountDelete(SearchAccountName);
				
	}
}

