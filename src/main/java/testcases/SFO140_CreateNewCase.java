package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.LoginPage;

public class SFO140_CreateNewCase extends PreAndPost {
	
	@BeforeTest
	public void setData() { 
		testCaseName = "SFO140_CreateNewCase";
		testDescription = "Ceate NewCase";
		authors = "Gomathi";
		category = "smoke";
		nodes = "Sales";
		dataSheetName = "CreateNewCash";
	} 
		
	@Test(dataProvider = "fetchData")
	public void createNewCase(String actName,String Subject,String Description,String Status) throws InterruptedException {
		new LoginPage(driver, test)
		.typeUserName("matschie@testleaf.com")
		.typePassword("Bootcamp$123")
		.clickLogIn()
		.clickAppLauncer()
		.clickViewAll()
		.clickSales()
		.clickMore()
		.clickCase()
		.clickNew()
		.clickAccountName(actName)
		.selectStatus(Status)
		.updateCaseOrgin()
		.enterSubject(Subject)
		.enterDescription(Description)
		.clickSave()
		.getCreatedCaseNumber()
		.clickCase()
		.searchCaseNumber()
		.getCountRow();
	}

}
