package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import lib.selenium.PreAndPost;
import pages.LoginPage;

public class SFO163_CertificationSortOrder extends PreAndPost{
	
	@BeforeTest
	public void setData() {
		testCaseName = "SFO163_CertificationSortOrder";
		testDescription = "Certifications Sort Order";
		authors = "Sree Poorani";
		category = "smoke";
		nodes = "Service";
	}
		
	@Test
	public void login() {
		new LoginPage(driver, test)
		.typeUserName("matschie@testleaf.com")
		.typePassword("Bootcamp$123")
		.clickLogIn()
		.clickSeeSystemGetStarted()
		.selectTrustCompliance()
		.sortAlphabetically()
		.verifyServicesSort();
	}
	

}
